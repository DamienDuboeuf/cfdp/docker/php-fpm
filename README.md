# Docker PHP FPM

[![pipeline status](https://gitlab.com/DamienDuboeuf/cfdp/docker/php-fpm/badges/master/pipeline.svg)](https://gitlab.com/DamienDuboeuf/cfdp/docker/php-fpm/-/commits/master)

Its docker image php fpm with manage UID and GUID and composer

## Usages

```bash
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm:7.0-fpm sh # For PHP FPM 7.0
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm:7.1-fpm sh # For PHP FPM 7.1
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm:7.2-fpm sh # For PHP FPM 7.2
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm:7.3-fpm sh # For PHP FPM 7.3
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm:7.4-fpm sh # For PHP FPM 7.4
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm:8.0-fpm sh # For PHP FPM 8.0
docker run -ti registry.gitlab.com/damienDuboeuf/cfdp/docker/php-fpm:8.1-fpm sh # For PHP FPM 8.1
```

use specifique tagged version

```bash
docker run -ti registry.gitlab.com/damienduboeuf/cfdp/docker/php-fpm:7.0-fpm-v1.0.0 sh # For PHP FPM 7.0
docker run -ti registry.gitlab.com/damienduboeuf/cfdp/docker/php-fpm:7.1-fpm-v1.0.0 sh # For PHP FPM 7.1
docker run -ti registry.gitlab.com/damienduboeuf/cfdp/docker/php-fpm:7.2-fpm-v1.0.0 sh # For PHP FPM 7.2
docker run -ti registry.gitlab.com/damienduboeuf/cfdp/docker/php-fpm:7.3-fpm-v1.0.0 sh # For PHP FPM 7.3
docker run -ti registry.gitlab.com/damienduboeuf/cfdp/docker/php-fpm:7.4-fpm-v1.0.0 sh # For PHP FPM 7.4
docker run -ti registry.gitlab.com/damienduboeuf/cfdp/docker/php-fpm:8.0-fpm-v1.0.0 sh # For PHP FPM 8.0
docker run -ti registry.gitlab.com/damienduboeuf/cfdp/docker/php-fpm:8.1-fpm-v1.0.0 sh # For PHP FPM 8.1
```

## Env vars

- `USER_ID=33` UID of php-fpm
- `USER_GID=33` GUID of php-fpm
- `COMPOSER_VERSION=` Set composer version