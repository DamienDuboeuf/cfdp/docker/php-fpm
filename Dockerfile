ARG PHP_VERSION=7.4

FROM php:${PHP_VERSION}-alpine
ARG COMPOSER_VERSION=2.2.7

MAINTAINER Damien DUBOEUF <dduboeuf@cfpd.fr>, Yann BLACHER <yblancher@cfpd.fr>

# Add shadow for change UUID, Add git for composer
run apk add --no-cache git shadow

#####################
# Add php installer #
#####################

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

############
# Composer #
############

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
ENV COMPOSER_VERSION=${1:+1}

########
# User #
########

ENV USER_ID=33
ENV USER_GID=33

COPY docker-user-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-user-entrypoint.sh
ENTRYPOINT ["docker-user-entrypoint.sh"]
CMD php-fpm